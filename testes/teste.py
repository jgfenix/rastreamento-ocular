# import cv2
#
# cap = cv2.VideoCapture(1)
#
# while True:
#     ret, frame = cap.read()
#     cv2.imshow("Frame", frame)
#     key = cv2.waitKey(1) & 0xFF
#
#     if key == ord("q"):
#         break
#     # time.sleep(0.05)
# cap.release()



# import numpy as np
import cv2
from PyQt5 import QtWidgets, uic
# from PyQt5.QtWidgets import QApplication, QWidget, QLabel
from PyQt5.QtGui import QPixmap
from threading import Thread
import time


valor = False


def FimCaptura():
    global valor
    valor = not valor
    dlg.label2.hide()

def Captura():
    cap = cv2.VideoCapture(1)
    dlg.label2.show()
    global valor
    while not valor:
        ret, frame = cap.read()
        cv2.imwrite("test3.png", frame)

        pixmap = QPixmap("test3.png")

        pixmap = pixmap.scaled(float(dlg.dial.value())*160/2, float(dlg.dial.value())*120/2)
        cv2.imshow("Frame", frame)

        dlg.label2.setPixmap(pixmap)
        time.sleep(0.05)
    cap.release()
    # time.sleep(4.0)


def StartCaptura():
    captura1 = Thread(target=Captura)
    captura1.start()


def Mostra():
    dlg.label.setText(str(dlg.dial.value()))


app = QtWidgets.QApplication([])
dlg = uic.loadUi("untitled.ui")

dlg.dial.valueChanged.connect(Mostra)
# dlg.label2.resize(320, 240)

dlg.pushButton.clicked.connect(StartCaptura)
dlg.pushButton2.clicked.connect(FimCaptura)

dlg.show()
app.exec()

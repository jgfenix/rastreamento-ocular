#programa que pega a posição do mouse na tela do 'jogo' e a imprime e desenha um círculo ao redor do mouse
#mexendo só o eixo
import pygame
import sys
import subprocess#, time
from pygame.locals import *

cmd = ['xrandr']
cmd2 = ['grep', '*']
p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
p2 = subprocess.Popen(cmd2, stdin=p.stdout, stdout=subprocess.PIPE)
p.stdout.close()

resolution_string, junk = p2.communicate()
resolution = resolution_string.split()[0]
screenWidth, screenHeight = str(resolution).split('x')
screenWidth = int(screenWidth[2:])
screenHeight = int(str(screenHeight).split("'")[0])
# acho que é o tamanho da resolucao da tela
print("width=", screenWidth, ",", "height=", screenHeight)


pygame.init()

#20% ??
escala = .20
WINDOW_WIDTH = int(screenWidth - escala * screenWidth)
WINDOW_HEIGHT = int(screenHeight - escala * screenHeight)
WHITE = (255, 255, 255)

### Creating the main surface ###
main_surface = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT), 0, 32)
background_colour = (255, 255, 255)
main_surface.fill(background_colour)
pygame.display.update()
# surface_rect = main_surface.get_rect()

# pygame.draw.circle(main_surface, (255, 0, 0), (int(WINDOW_WIDTH/2), int(WINDOW_HEIGHT/2)), 105, 10)
# pygame.display.update()
pygame.display.set_caption('pygame-test.py')
while True:
    for event in pygame.event.get():
        if event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                pygame.quit()
                sys.exit()
        else:
            pygame.draw.circle(main_surface, (255, 0, 0), [int(WINDOW_WIDTH/2), pygame.mouse.get_pos()[1]], 20, 10)
            pygame.display.update()
            main_surface.fill(background_colour)
            print(pygame.mouse.get_pos())
# while True:

# -----imports-----
import pygame
import time

import utils
import mouse
#tamanho do jogo sendo proporcional ao tamanho da tela
screenWidth, screenHeight = utils.getscreenresolution()
from os import environ
#gamePosXY = [screenWidth//8, screenHeight//2]
environ['SDL_VIDEO_CENTERED'] = '1'

#para rodar o Pong sozinho
if not environ.get('SDL_VIDEO_WINDOW_POS'):
    gamePosXY = [screenWidth//2, screenHeight//2]
else:
    gamePosXY = str(environ['SDL_VIDEO_WINDOW_POS']).split(sep=',')
    gamePosXY = str(environ['SDL_VIDEO_WINDOW_POS']).split(sep=',')

xGamePos = int(gamePosXY[0])
yGamePos = int(gamePosXY[1])

#print("xGamePos=",xGamePos,"|yGamePos=",yGamePos)

#20% ?? .20
escala = .8 #monitor Dell usando HDMI 1920x1080
gameDisplayWidth = int(screenWidth - escala * screenWidth)
gameDisplayHeight = int(screenHeight - escala * screenHeight)

global_ballSpeed = .3
################################################################

goals = [0, 0]

ownBlack = (0, 0, 0)
ownWhite = (255, 255, 255)
ownRed = (255, 0, 0)

# -----Initializing the game-------
def init():
    #print("x=",xGamePos+(gameDisplayWidth//2),"|y=",yGamePos+(gameDisplayHeight//2))
    mouse.move(xGamePos+(gameDisplayWidth//2), yGamePos+(gameDisplayHeight//2), absolute=True, duration=0)
    pygame.init()  # initialize pygame module
    pygame.display.set_caption("Pong")  # Set window Title

    # setar o x,y da posicao da tela do jogo
    # os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (x, y)

    screen = pygame.display.set_mode((gameDisplayWidth, gameDisplayHeight))  # Surface on screen with size of screenWidth x screenHeight
    # screen = pygame.display.set_mode((0,0), pygame.FULLSCREEN)
    settings(screen)
    time.sleep(1) #tempo para carregar a surface e depois centralizar o mouse
    # pygame.mouse.set_pos(WINDOW_WIDTH//2, WINDOW_HEIGHT//2)
    #pygame.mouse.set_pos(gameDisplayWidth // 2, gameDisplayHeight // 2)
    #time.sleep(1)

# -----Settings-----
def settings(screen):
    global global_ballSpeed
    #font = pygame.font.SysFont("arialroundedmtbold", 24)
    #settingsText = font.render("Settings", True, (255, 255, 255))
    #screen.blit(settingsText, (gameDisplayWidth//2-settingsText.get_width()//2, 10))
    #pygame.display.flip()

    # done = False
    # startgame = False
    startgame = True

    playerSpeed = 20        # Pixels the player moves per frame (speed)
    xstepB = global_ballSpeed          # "Speed" of the ball
    ystepB = 4
    playerNumber = 1

    #renderAndUpdate(screen, str(xstepB), ownWhite, ownBlack, (gameDisplayWidth//2, 20 + 20 + settingsText.get_height()))
    # time.sleep(0.01)
    #renderAndUpdate(screen, str(playerNumber), ownWhite, ownBlack, (gameDisplayWidth//2, 40 + 20 + settingsText.get_height()))
    # time.sleep(0.01)
    #renderAndUpdate(screen, str(playerSpeed), ownWhite, ownBlack, (gameDisplayWidth//2, 60 + 20 + settingsText.get_height()))


    if startgame:
        screen.fill((0, 0, 0))
        main(screen, playerNumber, xstepB, playerSpeed)


# -----Updating display with a text-----
def renderAndUpdate(screen, text, textColor, backgroundColor, pos):
    #font = pygame.font.SysFont("arialroundedmtbold", 24)
    #valueText = font.render(text, True, textColor, backgroundColor)
    #screen.blit(valueText, pos)
    #pygame.display.flip()
    return


# -----Update positions----- 
def updatePos(xPos, yPos, oldRect, screen, image):
    #preencher a tela toda de preto demandava muito tempo, agora preenche so a posicao antiga
    #screen.fill((0, 0, 0))
    screen.fill((0, 0, 0), oldRect)
    updatedRect = screen.blit(image, (xPos, yPos))
    #screen.fill((255, 255, 255), updatedRect)
    #font = pygame.font.SysFont("arialroundedmtbold", 18)
    #goalText = font.render(str(goals[0]) + " : " + str(goals[1]), True, (255, 255, 255), (0, 0, 0))
    pygame.display.update(updatedRect)
    pygame.display.update(oldRect)
    #pygame.display.update(screen.blit(goalText, (gameDisplayWidth / 2 - goalText.get_width() / 2, 10)))
    return updatedRect


# -----Main function--------------
def main(screen, playerCount, ballSpeed, playerSpeed):
    xstepB = ballSpeed
    ystepB = ballSpeed
    running = True  # Variable for main loop control

    playerImg = pygame.image.load("resources/player.png")  # Load the player image
    playerImg = pygame.transform.scale(playerImg, (int(gameDisplayHeight*.05), int(gameDisplayWidth*.25)))
    ballImg = pygame.image.load("resources/ball.png")
    ballImg = pygame.transform.scale(ballImg, (int(gameDisplayHeight * .05), int(gameDisplayHeight * .05)))

    screen.fill((0, 0, 0))  # Fill the background with one colour (black)

    playerWidth =  playerImg.get_width()             # Width of the player
    playerHeight = playerImg.get_height()           # Height of the player
    xpos1 = 1                                      # X-Position of the player 1
    xpos2 = gameDisplayWidth - xpos1 - playerWidth          # X-Position of the player 2
    ypos1 = gameDisplayHeight // 2 - playerHeight // 2   # Y-Position of the player 1
    ypos2 = ypos1                                   # Y-Position of the player 2


    ballWidth = ballImg.get_width()                 # Width of the ball
    ballHeight = ballImg.get_height()               # Height of the ball
    xposB = xpos1 + playerWidth + 10                # X-Position of the ball
    yposB = ypos1 + playerHeight // 2 - ballHeight // 2  # Y-Position of the ball


    lastRects = [screen.blit(playerImg, (xpos1, ypos1)),  # Which parts of the screen
                 screen.blit(playerImg, (xpos2, ypos2)),  # needs to be updated
                 screen.blit(ballImg, (xposB, yposB))]

    curRects = lastRects.copy()

    pygame.display.flip()  # Refresh the screen

    firstStart = False
    upPressed = False
    downPressed = False
    wPressed = False
    sPressed = False

    # -----main loop-----
    while running:
        x,y = mouse.get_position()
        # ---event handling, gets all events from event queue---
        # time.sleep(0.001)
        # if not firstStart:
        if (xposB >= gameDisplayWidth) or (xposB <= 0):
            firstStart = True
            if (xposB >= gameDisplayWidth):
                goals[0] += 1
            else:
                goals[1] += 1
            #time.sleep(2)
            xposB = gameDisplayWidth/2 - ballWidth/2 - xstepB
            yposB = gameDisplayHeight/2 - ballHeight/2 - ystepB
        if (yposB >= gameDisplayHeight) or (yposB <= 0):
            ystepB = -ystepB

        # Collision check with player 1
        if (xposB-3 <= (xpos1 + playerWidth)):

            if (yposB+ballHeight > ypos1) and (yposB < ypos1+playerHeight):
                #xAccelerator = 1 if xstepB > 1 else -1
                xstepB = (-xstepB)# - xAccelerator)
        if ((xposB + ballWidth)+3 >= xpos2):
            if (yposB+ballHeight > ypos2) and (yposB < ypos2+playerHeight):
                yAccelerator = 1 #if ystepB > 1 else -1
                xstepB = (-xstepB)# - yAccelerator)

            upDist = abs(yposB + ballHeight - ypos1)
            downDist = abs(yposB - ypos1 - playerHeight)
            ystepBAbs = abs(ystepB)

            if (xposB - xstepB - 3 <= (xpos1 + playerWidth) and (yposB + ballHeight >= ypos1 and yposB <= ypos1 + playerHeight)):
                # Ball entered through top or bottom and is still inside the paddle
                if (upDist < downDist):  # ball is above
                    ystepB = -ystepBAbs
                else:
                    ystepB = +ystepBAbs
            elif (yposB+ballHeight > ypos1) and (yposB < ypos1+playerHeight):
                xstepB = -xstepB

        # Collision check with player 2
        if ((xposB + ballWidth)+3 >= xpos2):
            upDist = abs(yposB + ballHeight - ypos2)
            downDist = abs(yposB - ypos2 - playerHeight)
            ystepBAbs = abs(ystepB)

            if (xposB + ballWidth - xstepB + 3 >= xpos2) and (yposB + ballHeight >= ypos2 and yposB <= ypos2 + playerHeight):
                # Ball entered through top or bottom and is still inside the paddle
                if (upDist < downDist):  # ball is above
                    ystepB = -ystepBAbs
                else:
                    ystepB = +ystepBAbs

            elif (yposB+ballHeight > ypos2) and (yposB < ypos2+playerHeight):
                xstepB = -xstepB

        xposB += xstepB
        yposB += ystepB
        curRects[2] = updatePos(xposB, yposB, lastRects[2], screen, ballImg)
        lastRects[2] = curRects[2]

        x_new, y_new = mouse.get_position()
        y = y_new - y
        #print("y=", y)
        #x, y = pygame.mouse.get_rel()

        if y == 0:
            sPressed = False
            wPressed = False
            # time.sleep(0.001)

        elif y > 0:  # (pygame.mouse.get_pos()[1] > WINDOW_HEIGHT / 2 + WINDOW_HEIGHT / 2 * .2):
            sPressed = True
            wPressed = False
            # time.sleep(0.001)
            # UP2 = False
            # DOWN2 = True
            # NO_MOVEMENT2 = False

        elif y < 0:  # elif (pygame.mouse.get_pos()[1] < WINDOW_HEIGHT / 2 - WINDOW_HEIGHT / 2 * .2):
            sPressed = False
            wPressed = True
            # time.sleep(0.001)
            # UP2 = True
            # DOWN2 = False
            # NO_MOVEMENT2 = False
        # time.sleep(int(abs(y)*.01))

        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                # if event.type == pygame.QUIT:   # Event type = quit:
                running = False             # Change running to False -> Main loop quits
                pygame.quit()

            # Switches firstStart variable if the game is started
            # if firstStart:
            #     if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
            #         firstStart = False

            # Switches the variables for the key pressed when they are pressed or released
            # if event.type == pygame.KEYDOWN and event.key == pygame.K_DOWN:
            #     downPressed = True
            # if event.type == pygame.KEYDOWN and event.key == pygame.K_UP:
            #     upPressed = True


            # --- Bot for one player gamming --- #
        if playerCount == 1:
            if yposB + 1 + playerSpeed > ypos2:
                #upPressed = False
                #downPressed = True
                ypos2 += playerSpeed
                curRects[1] = updatePos(xpos2, ypos2, lastRects[1], screen, playerImg)
                lastRects[1] = curRects[1]
            if yposB - 1 - playerSpeed < ypos2:
                #downPressed = False
                #upPressed = True
                ypos2 -= playerSpeed
                curRects[1] = updatePos(xpos2, ypos2, lastRects[1], screen, playerImg)
                lastRects[1] = curRects[1]

        # Actually moves the players if a key is pressed
        # First player

        # entender essa parte para mover o player1 de acordo com a posicao y do mouse
        if sPressed and ypos1 <= gameDisplayHeight - (playerHeight):#//2):
            ypos1 += playerSpeed
            curRects[0] = updatePos(xpos1, ypos1, lastRects[0], screen, playerImg)
            lastRects[0] = curRects[0]
        if wPressed and ypos1 >= 0 + playerSpeed :#- (playerHeight//2):
            ypos1 -= playerSpeed
            curRects[0] = updatePos(xpos1, ypos1, lastRects[0], screen, playerImg)
            lastRects[0] = curRects[0]


        # Second player
        if playerCount == 2:
            if downPressed and ypos2 <= gameDisplayHeight - (playerHeight):#//2):
                ypos2 += playerSpeed
                curRects[1] = updatePos(xpos2, ypos2, lastRects[1], screen, playerImg)
                lastRects[1] = curRects[1]
            if upPressed and ypos2 >= 0 + playerSpeed :#- (playerHeight):#//2):
                ypos2 -= playerSpeed
                curRects[1] = updatePos(xpos2, ypos2, lastRects[1], screen, playerImg)
                lastRects[1] = curRects[1]


if __name__ == "__main__":  # Only if the script is called as main script not if its imported as a module
    init()  # Call init function

from PyQt5 import QtWidgets, uic
from PyQt5.QtGui import QPixmap
from threading import Thread
import time
# import the necessary packages
import numpy as np
# import matplotlib.pyplot as plt
# import argparse
import cv2
import mouse

#pegar a resolucao da tela pra mapear
import subprocess

cmd = ['xrandr']
cmd2 = ['grep', '*']
p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
p2 = subprocess.Popen(cmd2, stdin=p.stdout, stdout=subprocess.PIPE)
p.stdout.close()

resolution_string, junk = p2.communicate()
resolution = resolution_string.split()[0]
width, height = str(resolution).split('x')

print("width=",width,",","height=",height)

valor = False


def FimCaptura():
    global valor
    valor = not valor
    dlg.label2.hide()


def Captura():
    cap = cv2.VideoCapture(-1)
    dlg.label2.show()
    global valor
    while not valor:
        ret, frame = cap.read()
        cv2.imwrite("test3.png", frame)
        pixmap = QPixmap("test3.png")
        # pixmap = pixmap.scaled(float(dlg.dial.value())*160/2, float(dlg.dial.value())*120/2)

        image = cv2.imread("test3.png")
        output = image.copy()
        gray = cv2.cvtColor(output, cv2.COLOR_BGR2GRAY)


        # detect circles in the image
        # circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, 1.5, 10000, param1=50 \
        #                            , param2=30, minRadius=35, maxRadius=50)
        circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, 1.5, 10000, param1=50 \
                                   , param2=30, minRadius=35, maxRadius=50)


        if circles is not None:
            # convert the (x, y) coordinates and radius of the circles to integers
            circles = np.round(circles[0, :]).astype("int")
            x, y, r = circles[0]
            print("X=", x, " Y=", y)
            cv2.circle(output, (x, y), r, (0, 255, 0), 4)
            cv2.rectangle(output, (x - 5, y - 5), (x + 5, y + 5), (0, 128, 255), -1)
            # mouse.move(x, y, absolute=False, duration=0)
            cv2.imwrite("test4.png", output)
            pixmap = QPixmap("test4.png")
            print("OLHO NAO DETECTADO")
        else:
            # pixmap = QPixmap("1.png")
            print("nao achou")
        # time.sleep(0.05)
        pixmap = pixmap.scaled(float(dlg.dial.value()) * 160 / 2, float(dlg.dial.value()) * 120 / 2)
        dlg.label2.setPixmap(pixmap)

    cap.release()
    # time.sleep(4.0)


def StartCaptura():
    captura1 = Thread(target=Captura)
    captura1.start()


def Mostra():
    dlg.label.setText(str(dlg.dial.value()))


app = QtWidgets.QApplication([])
dlg = uic.loadUi("untitled.ui")

dlg.dial.valueChanged.connect(Mostra)
# dlg.label2.resize(320, 240)

dlg.pushButton.clicked.connect(StartCaptura)
dlg.pushButton2.clicked.connect(FimCaptura)

dlg.show()
app.exec()

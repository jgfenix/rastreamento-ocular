# falta resolver a exception ao criar a thread com uma camera invalida
# anotar todas as bibliotecas qe versões que estao sendo utilizadas aqui
# >>> import pkg_resources
# >>> pkg_resources.get_distribution("mouse").version
# '0.7.1'
# >>> import cv2
# >>> cv2.__version__
# '4.1.2'
# >>> pkg_resources.get_distribution("imutils").version
# '0.5.3'
# >>> pkg_resources.get_distribution("PyQt5").version
# '5.11.3'

#import faulthandler; faulthandler.enable()

from PyQt5 import QtWidgets, uic
from PyQt5.QtGui import QPixmap, QImage
from threading import Thread

import cv2
import mouse
import subprocess
import sys
import time
import csv

import logging
#logging.basicConfig(level=logging.INFO)

import utils
######### BEGIN CAMERA CONFIG #########
cameraHeight = 640//2#640
cameraWidth  = 480//2#240
runningCV2 = False
currentCamera = -1
mirror = -2
startStopCamera = False
######### END CAMERA CONFIG #########


pongGameOn = False

# aumentando o quanto o mouse vai se deslocar proporcionalmente ao movimento do olho
# ideal é colocar um botao pra calibrar
PROPORCAO = 1

screenWidth, screenHeight = utils.getscreenresolution()

# UI
app = QtWidgets.QApplication([])
dlg = uic.loadUi("interfaceGraficaPrograma.ui")

# @profile
def Captura(camera):
    global pongGameOn
    global cameraHeight
    global cameraWidth
    # https: // docs.opencv.org / master / dd / d43 / tutorial_py_video_display.html
    cap = cv2.VideoCapture(int(camera))  # "video_olho_webcam_gravado.mp4")  # int(camera))

    ret = cap.set(cv2.CAP_PROP_FRAME_WIDTH, cameraWidth)
    if not ret:
        exit("ERROR ON cv2.CAP_PROP_FRAME_WIDTH ", cameraWidth)
    ret = cap.set(cv2.CAP_PROP_FRAME_HEIGHT, cameraHeight)
    if not ret:
        exit("ERROR ON cv2.CAP_PROP_FRAME_HEIGHT ", cameraHeight)

    frame_width =  int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    frame_height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

    # logging.info("wCam=%s|hCam=", frame_width, frame_height)

    # pegar esse erro aqui em caso de nao abrir a camera e mostrar um popup
    # if not cap.isOpened():  # check if we succeeded
    # logging.info("CAMERA %s NAO DISPONIVEL", camera)
    dlg.displayCameraFrame.show()
    global runningCV2

    # https://doc.qt.io/qt-5/qimage.html#Format-enum
    q_image_format = QImage.Format_RGB888  # Format_RGB888
    # MEDIA = 0
    while runningCV2:
        # definicoes de deteccao de circulo antes de capturar a imagem
        dp_ = dlg.dpSlider.value() / 10
        minDist_ = dlg.minDistlSlider.value()
        param1_ = dlg.param1Slider.value()
        param2_ = dlg.param2Slider.value()
        minRadius_ = dlg.minRadiusSlider.value()
        maxRadius_ = dlg.maxRadiusSlider.value()

        ret, frame = cap.read()
        # testa se nao tem um frame
        if not ret:
            continue

        # flip frame
        if mirror != -2: #defini como -2 nao fazer nada
                frame = cv2.flip(frame, mirror)

        #zoom
        scale = 1 / dlg.zommDial.value()

        #crop da imagem que recebeu o zoom
        centerX, centerY = int(frame_height / 2), int(frame_width / 2)
        radiusX, radiusY = int(centerX*scale), int(centerY*scale)
        minX, maxX = centerX - radiusX, centerX + radiusX
        minY, maxY = centerY - radiusY, centerY + radiusY
        frame = frame[minX:maxX, minY:maxY]
        frame = cv2.resize(frame, (frame_width, frame_height))

        # testando blur
        # https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_filtering/py_filtering.html
        # frame = cv2.bilateralFilter(frame, 9, 9, 75)
        image = QImage(frame, frame_width, frame_height, q_image_format)  # Format_RGB888

        #exibindo a imagem com o círculo na interface gráfica
        pixmap = QPixmap.fromImage(image)

        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, dp_, minDist_, param1=param1_, param2=param2_,
                                   minRadius=minRadius_, maxRadius=maxRadius_)

        if circles is not None:
            # convert the (x, y) coordinates and radius of the circles to integers
            x, y, r = round(circles[0,0][0]), round(circles[0,0][1]), round(circles[0,0][2])
            #logging.info("X=%s|Y=", x, y)
            cv2.circle(frame, (x, y), r, (0, 255, 0), 2)
            cv2.rectangle(frame, (x - 4, y - 4), (x + 4, y + 4), (0, 128, 255), -1)
            # REATIVAR AS LINHAS ABAIXO PARA MOVER O MOUSE
            if pongGameOn: #and MEDIA >= 2:
                mouseX, mouseY = mouse.get_position()
                """PAREI DE MOVER NO EIXO X mouse.move(utils.Map(x, 0, frame_width, 0, screenWidth * PROPORCAO)"""
                mouse.move(mouseX,
                           utils.Map(y, 0, frame_height, 0, screenHeight * PROPORCAO), absolute=True)#, duration=0.015)
                # MEDIA = 0
            # else:
            #     MEDIA = MEDIA + 1

            image_tmp = QImage(frame, frame_width, frame_height, q_image_format)

            # testar com pixmap = QPixmap.convertFromImage(image_tmp) e outros parametros pra ver qual o mais rapido
            pixmap = QPixmap.fromImage(image_tmp)

            # Y=("OLHO DETECTADO")
        # else:
        # logging.info("nao achou")
        # time.sleep(0.05)
        dlg.displayCameraFrame.setPixmap(pixmap)
    cap.release()


def StartCaptura():
    global currentCamera
    global runningCV2
    runningCV2 = not runningCV2
    logging.info("runningCV2=%s", runningCV2)
    captura1 = Thread(target=Captura, args=currentCamera)
    captura1.start()


def FimCaptura():
    global runningCV2
    runningCV2 = not runningCV2
    logging.info("runningCV2=%s", runningCV2)
    time.sleep(.4)
    #para nao terminar com o ultimo frame processado, carregar e exibir novamente a imagem padrão
    pixmap = QPixmap("resources/olho_imagem_fundo.png")
    dlg.displayCameraFrame.setPixmap(pixmap)


def StartStopCamera():
    global startStopCamera

    if not startStopCamera:
        StartCaptura()
        dlg.StartStopCameraButton.setText("Desligar camera")
    else:
        FimCaptura()
        dlg.StartStopCameraButton.setText("Ligar camera")

    startStopCamera = not startStopCamera


def FecharPrograma():
    logging.info("Fechando programa...")
    FimCaptura()
    SaveParamsOnCSV(utils.getCSVFileName())
    app.quit()


def MirrorImage():
    global mirror
    mirror+=1
    if mirror > 1:
        mirror = -2
    logging.info("global mirror=%s", mirror)


def updateScreenValues():
    """atualiza os valores numericos na interface visual do programa"""
    dlg.label.setText(str(dlg.zommDial.value()))
    dlg.dpValueLabel.setText(str(dlg.dpSlider.value()))
    dlg.minDistValueLabel.setText(str(dlg.minDistlSlider.value()))
    dlg.param1ValueLabel.setText(str(dlg.param1Slider.value()))
    dlg.param2ValueLabel.setText(str(dlg.param2Slider.value()))
    dlg.minRadiusValueLabel.setText(str(dlg.minRadiusSlider.value()))
    dlg.maxRadiusValueLabel.setText(str(dlg.maxRadiusSlider.value()))


def SaveParamsOnCSV(fileName):
    logging.info("Salvando parametros!")
    global mirror
    headerFields = ['zommDial','dpSlider','minDistlSlider','param1Slider','param2Slider','minRadiusSlider','maxRadiusSlider','mirrorRotateImage']
    myFile = open(fileName, 'w')
    with myFile:
        writer = csv.DictWriter(myFile, fieldnames=headerFields)
        writer.writeheader()
        writer.writerow({headerFields[0]    : dlg.zommDial.value(),
                         headerFields[1]    : dlg.dpSlider.value(),
                         headerFields[2]    : dlg.minDistlSlider.value(),
                         headerFields[3]    : dlg.param1Slider.value(),
                         headerFields[4]    : dlg.param2Slider.value(),
                         headerFields[5]    : dlg.minRadiusSlider.value(),
                         headerFields[6]    : dlg.maxRadiusSlider.value(),
                         headerFields[7]    : mirror})


def GetParamsFromCSV(fileName):
    if not utils.testaARquivoExiste(fileName):
        logging.info("usando valores defult!")
        dlg.zommDial.setValue(1)
        dlg.dpSlider.setValue(10)
        dlg.minDistlSlider.setValue(1390)
        dlg.param1Slider.setValue(17)
        dlg.param2Slider.setValue(35)
        dlg.minRadiusSlider.setValue(11)
        dlg.maxRadiusSlider.setValue(40)

    else:
        global mirror
        dictFromCSV = []
        with open(fileName) as csv_file:
            csv_reader = csv.DictReader(csv_file, delimiter = ',')
            for row in csv_reader:
                dictFromCSV.append(row)

            dictFromCSV = dictFromCSV[0] #return dict  {'maxRadiusSlider': '51', 'param1Slider': '17', ...
            logging.info("dictFromCSV=\n%s", dictFromCSV)

            if 'zommDial' in dictFromCSV:
                dlg.zommDial.setValue(int(dictFromCSV["zommDial"]))
                logging.info('zommDial=%s',dictFromCSV["zommDial"])

            if 'dpSlider' in dictFromCSV:
                dlg.dpSlider.setValue(int(dictFromCSV['dpSlider']))
                logging.info('dpSlider=%s',dictFromCSV['dpSlider'])

            if 'minDistlSlider' in dictFromCSV:
                dlg.minDistlSlider.setValue(int(dictFromCSV['minDistlSlider']))
                logging.info('minDistlSlider=%s',dictFromCSV['minDistlSlider'])

            if 'param1Slider' in dictFromCSV:
                dlg.param1Slider.setValue(int(dictFromCSV['param1Slider']))
                logging.info('param1Slider=%s',dictFromCSV['param1Slider'])

            if 'param2Slider' in dictFromCSV:
                dlg.param2Slider.setValue(int(dictFromCSV['param2Slider']))
                logging.info('param2Slider=%s',dictFromCSV['param2Slider'])

            if 'minRadiusSlider' in dictFromCSV:
                dlg.minRadiusSlider.setValue(int(dictFromCSV['minRadiusSlider']))
                logging.info('minRadiusSlider=%s',dictFromCSV['minRadiusSlider'])

            if 'maxRadiusSlider' in dictFromCSV:
                dlg.maxRadiusSlider.setValue(int(dictFromCSV['maxRadiusSlider']))
                logging.info('maxRadiusSlider=%s',dictFromCSV['maxRadiusSlider'])

            if 'mirrorRotateImage' in dictFromCSV:
                mirror = int(dictFromCSV['mirrorRotateImage'])
                logging.info('mirrorRotateImage=%s',int(dictFromCSV['mirrorRotateImage']))

    updateScreenValues()


# IDEIA: MOSTRAR UM POP UP DE QUE ESTA CARREGANDO O PROGRAMA COM ALGUMA ANIMAÇÃO RODANDO
def refreshCamList():
    dlg.selectedCameracomboBox.clear()
    usbWebCamList = utils.listWebCams()
    for webCam in usbWebCamList:
        dlg.selectedCameracomboBox.addItem(webCam)


#TODO: cada vez que clicar na lista de cameras, atualizar a lista!
def setaWebCam():
    global currentCamera
    currentCamera = dlg.selectedCameracomboBox.currentText()


pongProcessChild = 0
def iniciaEncerraPong():
    global pongGameOn
    global pongProcessChild
    if (pongGameOn == True):
        logging.info("Fechando pong...")
        pongGameOn = False
        pongProcessChild.kill()
        dlg.pongButton.setText("Iniciar jogo Pong!")
        # p2__.stdout.close()

    else:
        logging.info("Abrindo pong...")
        #utils.setPongAnchor(screenWidth, screenHeight)
        #movendo o mouse para o meio da tela do jogo ANTES de abri-lo
        time.sleep(.1)
        mouse.move(screenWidth//2, screenHeight//2, absolute=True, duration=0)
        pongProcessChild = subprocess.Popen([sys.executable, "pong.py"])
        pongGameOn = True
        dlg.pongButton.setText("Fechar Pong")


dlg.zommDial.valueChanged.connect(updateScreenValues)
dlg.dpSlider.valueChanged.connect(updateScreenValues)
dlg.minDistlSlider.valueChanged.connect(updateScreenValues)
dlg.param1Slider.valueChanged.connect(updateScreenValues)
dlg.param2Slider.valueChanged.connect(updateScreenValues)
dlg.minRadiusSlider.valueChanged.connect(updateScreenValues)
dlg.maxRadiusSlider.valueChanged.connect(updateScreenValues)

refreshCamList()
currentCamera = dlg.selectedCameracomboBox.currentText()

dlg.selectedCameracomboBox.currentIndexChanged.connect(setaWebCam)
dlg.refreshCameraList.clicked.connect(refreshCamList)
dlg.StartStopCameraButton.clicked.connect(StartStopCamera)

dlg.pongButton.setText("Iniciar jogo Pong!")
dlg.pongButton.clicked.connect(iniciaEncerraPong)
dlg.mirrorButton.clicked.connect(MirrorImage)

GetParamsFromCSV(utils.getCSVFileName())

setaWebCam()
dlg.show()
app.aboutToQuit.connect(FecharPrograma)
app.exec()

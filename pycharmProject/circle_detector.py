# import the necessary packages
import numpy as np
import matplotlib.pyplot as plt
# import argparse
import cv2
import mouse

#pegar a resolucao da tela pra mapear
import subprocess

# cmd = ['xrandr']
# cmd2 = ['grep', '*']
# p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
# p2 = subprocess.Popen(cmd2, stdin=p.stdout, stdout=subprocess.PIPE)
# p.stdout.close()
#
# resolution_string, junk = p2.communicate()
# resolution = resolution_string.split()[0]
# width, height = str(resolution).split('x')
#
# print("width=",width,",","height=",height)

# load the image, clone it for output, and then convert it to grayscale

image = cv2.imread("screenshots/1.jpg")
output = image#image.copy()
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
# th = cv2.adaptiveThreshold(gray,255,cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY,11,2)
# detect circles in the image
# circles = cv2.HoughCircles(th, cv2.HOUGH_GRADIENT, 1.5, 10000, param1=50, param2=30, minRadius=35, maxRadius=50)
circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, 10.9, minDist=1, param1=100, param2=100, minRadius=70, maxRadius=100)

# ensure at least some circles were found
if circles is not None:
    # convert the (x, y) coordinates and radius of the circles to integers
    circles = np.round(circles[0, :]).astype("int")

    # loop over the (x, y) coordinates and radius of the circles
    # for (x, y, r) in circles:
        # draw the circle in the output image, then draw a rectangle
        # corresponding to the center of the circle
        # cv2.circle(output, (x, y), r, (0, 255, 0), 4)
    x,y,r = circles[0]
    cv2.rectangle(output, (x - 5, y - 5), (x + 5, y + 5), (0, 128, 255), -1)
    print("X=",x, " Y=",y)
    mouse.move(x, y, absolute=True, duration=0)
    # show the output image
    # cv2.imshow("output", np.hstack([image, output]))
    # cv2.waitKey(0)
    plt.plot([x-20,x+20], [y-20,y+20], 'go-', label='line 1', linewidth=2)
    # plt.plot(x-10, y-10, 'bo') #traco azul
    # plt.plot(x + 10, y + 10, 'bo') #traco azul
    plt.imshow(output,cmap='gray',interpolation='bicubic')
    plt.show()
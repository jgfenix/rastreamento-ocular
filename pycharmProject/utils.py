# Dom Mar  1 19:10:27 -03 2020

import cv2
# import sys
import subprocess
import os
import time
import logging
from threading import Thread
import datetime

#logging.basicConfig(level=logging.INFO) #DEBUG

CONFIG_FILE = "config.txt"
CSV_FILE = ""

# https://www.pyimagesearch.com/2015/12/21/increasing-webcam-fps-with-python-and-opencv/
class FPS:
    def __init__(self):
        # store the start time, end time, and total number of frames
        # that were examined between the start and end intervals
        self._start = None
        self._end = None
        self._numFrames = 0

    def start(self):
        # start the timer
        self._start = datetime.datetime.now()
        return self

    def stop(self):
        # stop the timer
        self._end = datetime.datetime.now()

    def update(self):
        # increment the total number of frames examined during the
        # start and end intervals
        self._numFrames += 1

    def elapsed(self):
        # return the total number of seconds between the start and
        # end interval
        return (self._end - self._start).total_seconds()

    def fps(self):
        # compute the (approximate) frames per second
        return self._numFrames / self.elapsed()


class WebcamVideoStream:
    def __init__(self, src=0):
        # initialize the video camera stream and read the first frame
        # from the stream
        self.stream = cv2.VideoCapture(src)
        (self.grabbed, self.frame) = self.stream.read()
        # initialize the variable used to indicate if the thread should
        # be stopped
        self.stopped = False

    def start(self):
        # start the thread to read frames from the video stream
        Thread(target=self.update, args=()).start()
        return self

    def update(self):
        # keep looping infinitely until the thread is stopped
        while True:
            # if the thread indicator variable is set, stop the thread
            if self.stopped:
                return
            # otherwise, read the next frame from the stream
            (self.grabbed, self.frame) = self.stream.read()

    def read(self):
        # return the frame most recently read
        return self.frame

    def stop(self):
        # indicate that the thread should be stopped
        self.stopped = True

    def width(self):
        return self.stream.get(cv2.CAP_PROP_FRAME_WIDTH)

    def height(self):
        return self.stream.get(cv2.CAP_PROP_FRAME_HEIGHT)

# MUITO LENTO NO RASPBERRY, PRECISA MELHORAR
def testaCameraDisponivel1(cam):
    """
    :param cam: the camera to test
    :return:    True or False
    """
    # testando se a camera esta disponivel para usar, mas ta esquisito ainda, deve ter como fazer mais facil
    command = "python3 teste-camera.py " + cam + " && echo $?"
    saida = subprocess.check_output(command, stderr=subprocess.STDOUT, shell=True)
    # print("SAIDA.size=", len(saida))
    if len(saida) > 2:
        logging.info("CAMERA %s NAO DISPONIVEL", cam)
        return False
    else:
        logging.info("CAMERA %s disponivel", cam)
        return True


def testaCameraDisponivel2(cam):
    """
    :param cam: the camera to test
    :return:    True or False
    """
    cap = cv2.VideoCapture(cam)
    ret = cap.grab()
    if not ret:
        logging.info("CAMERA %s NAO DISPONIVEL", cam)
        return False
    else:
        logging.info("CAMERA %s disponivel", cam)
        return True


# vale a pena fazer uma funcao pra windows ou renomear essa pra conter a palavra linux?
def listWebCams():
    """
    Listing webcams attached in USB - only works on linux
    :return:    list of attached cams in USB
    """
    usb_webcam_list_command = "ls -ltrh /dev/video? | awk '{print $10}' | awk  -F video '{print $2}'"
    usb_webcam_list = os.popen(usb_webcam_list_command).read()
    usb_webcam_list = usb_webcam_list.split()
    logging.info("usb_webcam_list= %s", usb_webcam_list)
    true_webcam_list = []
    for webCam in usb_webcam_list:
        if testaCameraDisponivel2(int(webCam)):
            true_webcam_list.append(webCam)

    logging.info("true_webcam_list= %s", true_webcam_list)
    return true_webcam_list


def getscreenresolution():
    cmd = ['xrandr']
    cmd2 = ['grep', '*']
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    p2 = subprocess.Popen(cmd2, stdin=p.stdout, stdout=subprocess.PIPE)
    p.stdout.close()

    resolution_string, junk = p2.communicate()
    p2.stdout.close()
    resolution = resolution_string.split()[0]
    screen_width, screen_height = str(resolution).split('x')
    screen_width = int(screen_width[2:])
    screen_height = int(str(screen_height).split("'")[0])
    logging.info("width=%s, height=%s", screen_width, screen_height)
    return screen_width, screen_height

def testaARquivoExiste(nomeArquivo):
    if os.path.isfile(nomeArquivo):
        logging.info("%s existe",nomeArquivo)
        return True
    logging.info("%s nao existe!!",nomeArquivo)
    return False

#ler arquivo com as possiveis configuracoes salvas, tipo tamanho de tela, nome do csv
def LerArquivoConfig():
    global CSV_FILE
    if testaARquivoExiste(CONFIG_FILE):
        f = open(CONFIG_FILE, 'r')
        line = f.readline()
        CSV_FILE = line.rsplit("=")[1].replace('\n','')
        logging.info("CSV_FILE=%s", CSV_FILE)
        f.close()


def getCSVFileName():
    return CSV_FILE


def Map(x, in_min, in_max, out_min, out_max):
    """
    Re-maps a number from one range to another, inspired on Arduino's map function
    https://www.arduino.cc/reference/en/language/functions/math/map/
    :param x: the value to be mapped
    :param in_min:  minimum value that x could assume
    :param in_max:  maximum value that x could assume
    :param out_min: minimum value of another range
    :param out_max: maximum value of another range
    :return:        equivalent value in another range
    """
    return int((x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min)


def setPongAnchor(screenWidth, screenHeight):
    logging.info("screenWidth=%s, screenHeight=%s",screenWidth, screenHeight)
    os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (screenWidth//2, screenHeight//2)


LerArquivoConfig()
    #if testaARquivoExiste("config.txt"):
# def beginTimeCounter():
# """CONTANDO TEMPO INICIO"""
# start_time = time.time()
# """TRECHO A SER MEDIDO:"""
# frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
#
# print("{:.10f}".format(time.time() - start_time))
# time.sleep(.3)
# """CONTANDO TEMPO FIM"""

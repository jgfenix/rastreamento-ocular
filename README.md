# rastreamento-ocular

HOW-TO:

Executar o programa:
	Abrir um terminal nesta pasta e executar o comando:

	python3 video-circle-detector2.py

Ordem genérica de uso:
	1 - Selecionar webcam;
	2 - Clicar no botão "Ligar Câmera";
	3 - Ajustar o Zoom da webcam pela interface;
	4 - Ajustar os outros parâmetros até que o círculo desejado esteja ao redor de um círculo de borda verde e com ponto azul no centro do círculo;
	5 - Clicar no botão "Iniciar jogo Pong!";
	6 - Abrirá e iniciará o jogo em uma janela à parte, agora é só mexer o seu olho/círculo para cima e para baixo(somente considero a variação no eixo Y)

Caso esteja numa VM VirtualBox:
	Testei na versão 5.1.38 no Ubuntu 16.04 LTS, a versão 6.xx estava com bug para conectar a webcam ou usar a câmera do notebook. A VM que estou usando tem a seguinte configuração no VirtualBox:
		- 2048 MB de RAM
		- 2 CPUs
		- Rede em NAT

	Instalar o Extension Pack para sua versão do VirtualBox (links no final);
	
	Verificar na VM, se a aba 
		USB > Controladora USB 2.0 
	está selecionada, se não, selecionar;

	Iniciar a VM, depois selecionar a câmera para a VM em
		Dispositivos > WebCams
	e selecionar a câmera desejada.

	Se for a VM que disponibilizei:
		usuário: osboxes
		senha:   osboxes.org

		local do projeto ~/rastreamento-ocular/pycharmProjects
		sempre dê um 
			git pull
		antes de utilizar

		*Essa VM não pode enviar alterações pro git


Para fazer o download do repositório sem poder alterar, mas receber atualizações:
	git clone https://VM_OSBOXES:TBqriosszYhtVvzhdahA@gitlab.com/jgfenix/rastreamento-ocular.git

VirtualBox 5.1 e extension pack
	https://www.virtualbox.org/wiki/Download_Old_Builds_5_1